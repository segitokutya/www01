---
title: "Mennyi ideig tart egy segítőkutya kiképzése?"
date: "2020-07-07"
description: "A kutya kiképzésének ideje függ annak céljától és szerepétől, de más körülmény is hatással lehet rá."
category: "GYIK"
tags:
  - gyik
---

Nagyságrendileg, terápiás vagy személyi segítő kutya 12-15 hónapon át van képezve. 
Mozgássérült segítő és a jelzőkutyák pedig 15-18 hónap.
