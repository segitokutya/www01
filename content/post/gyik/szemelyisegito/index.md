---
title: "Mi a személyi segítőkutya?"
date: "2020-05-21"
description: "Ez az egyéni terápiás kutya!"
category: "GYIK"
tags:
  - gyik
---

Személyi segítő kutyát igényelhet, ha Ön vagy gyermeke:

- szorong emberek társaságában
- nem tud, vagy nem akar megszólalni, beszélni
- nehezen ért, vagy jegyez meg dolgokat
- nem tudja, vagy nem akarja követni a közösség szabályait
- nehezen alakít ki szociális kapcsolatokat
- túlzottan zárkózott, fél egyedül kimenni a lakásból
- egyensúlyával, vagy mozgásával problémák vannak, fejlesztésre szorul
- kedélyállapota indokolatlanul erősen ingadozó (depressziós)
- fél az állatoktól

Személyi segítő kutya [igénylés folyamata itt](/jelentkezes/segitokutya-igenyles).

### Miben tud segíteni a Személyi segítő kutya?

Elsősorban biztonságot nyújt a gazda-kutya közötti kötődés kialakulása után olyan plusz motivációt ad gazdájának, hogy a kutya kedvéért olyan dolgokat is megtesz a gazdi, amit kutya nélkül nem, egyáltalán nem, vagy nem olyan kitartóan, nem olyan gyakorisággal és intenzitással tesz. Segít a kapcsolatteremtésben, szabálykövetésben, biztonságot ad jelenléte a gazdinak. Így gyorsabbá, hatékonyabbá válnak a fejlesztéssel is.

Részletesebben olvashat [róluk itt](/tudastar/szemelyisegito).
