---
title: "Miben tud segíteni a terápiás személyi segítő, mozgássérült segítő és a jelző kutya?"
date: "2020-05-21"
description: "Sok mindenben..."
category: "GYIK"
tags:
  - gyik
---

Röviden:
* mozgáskorlátozott személyt segítő kutya: a mozgáskorlátozott személyt mindennapi tevékenységeinek ellátásában segítő feladatokra kiképzett kutya,
* hangot jelző kutya: a hallássérült személy számára veszélyt vagy egyéb fontos információt jelentő hangok jelzésére kiképzett kutya,
* rohamjelző kutya: az epilepsziával élő személy vagy más krónikus, rohamszerű állapotoktól veszélyeztetett személy számára a roham során segítséget nyújtó feladatokra kiképzett kutya,
* személyi segítő kutya: a fogyatékos személyt önálló életvitelében segítő feladatokra kiképzett kutya,
* terápiás kutya: a gyógypedagógiai, a szociális szolgáltatások területén pedagógiai, pszichológiai, pszichiátriai, konduktív pedagógiai habilitációs, illetve rehabilitációs folyamatban alkalmazott kutya
