---
title: "Mi az a terápiás kutya?"
date: "2020-05-21"
description: "Terápiás foglalkozásokhoz képzett kutyák."
category: "GYIK"
tags:
  - gyik
---

A terápiás kutyákat nem adjuk családokhoz, a kutya az egyén életében a kiképzőjével lakik. Együtt járnak fejlesztő foglalkozásokat tartani. Fejlesztő foglalkozás történhet csoportosan vagy egyénileg. A foglalkozást szakember irányítja (pszichológus, gyógypedagógus, konduktor, pedagógus, gyógytornász stb.), a terápiás kutyát a foglalkozáson a felvezető irányítja.

Terápiás foglalkozások célja lehet mentális és/vagy kognitív képességek fejlesztése, illetve mozgásfejlesztés is lehet cél.
Jelentkezni kutyás foglalkozásokra itt.

Részletesebben olvashat [róluk itt](/tudastar/terapias).
