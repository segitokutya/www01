---
title: "Képzések indulnak!"
date: "2020-11-29"
author: "Tamás"
description: "Az AURA Segítőkutya Alapítvány új képzéseket indít."
category: "Hírek"
tags:
  - hirek
  - kepzes

---

Az AURA új képzéseket indít! Bővebb informácio itt: [Képzések](/jelentkezes/kepzes/).
