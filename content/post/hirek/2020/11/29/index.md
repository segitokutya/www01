---
title: "Kutyaiskola és panzió!"
date: "2020-11-29"
author: "Tamás"
description: "Az AURA Segítőkutya Alapítvány új szolgáltatása."
category: "Hírek"
tags:
  - hirek
  - kutyasuli
  - szolgaltatas

---

Az AURA új szolgáltatást tett elérhetővé, itt a [Kutyasuli](/kutyasuli/)!
