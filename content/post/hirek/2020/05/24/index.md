---
title: "Megújul a honlapunk (második rész)"
date: "2020-05-24"
author: "Tamás"
description: "További infó a honlapról"
category: "Hírek"
tags:
  - hirek
  - honlap
---

Az AURA Segítőkutya Alapítvány új honlapja már majdnem kész.
Sajnos több rész még nem kész, ott az alábbi "feltöltés alatt"
helyztük el.

### Feltöltés alatt

----

![Feltöltés alatt](../../../../../../src/images/photo/under-construction.jpg)

----
