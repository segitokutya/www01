---
title: "Támogatás"
date: "2020-08-13"
description: "Segíts minket és munkánkat!"
category: "Támogatás"
tags:
  - tamogatas
---

Az AURA Segítő Kutya Alapítványt és munkánktat többféleképpen 
is Tudod támogatni.

## Adó 1%

Adószámunk: **18995736-1-09**

Támogasd az AURA alapítványt személyi jövedelem adód egy százalékával. 
További útmutatót a támogatáshoz a 
[NAV oldalán találhat](https://www.nav.gov.hu/nav/szja/szja).

## AURA Önkéntes Program

Az AURA önkénteseket mindig szívesen vár. Az önkéntesek különféle
programokban vehetnek részt, mindenki segítségét, akár más
szakterület szakértelmét is szívesen várjuk! 

## Támogatóink

- GB Capital Partners Vagyonkezelő Zrt.
- Matchmaker Capital Kft.
- 4LINE Kereskedelmi és Szolgáltató Kft.
- Flat White Számítástechnikai Kft.
- Számtan-Tax Kft.
- Weninger Bt.
- Janssen-Cilag Kft.
- NP Audit Tanácsadó Kft.
- BrekSys Bt.
- Nemzeti Szociálpolitikai Intézet

![NSZI](nemszocpolint.jpeg)
![BGA](bga_alap_logo.jpg)
![ME](miniszterelnokseg_logo.jpg)