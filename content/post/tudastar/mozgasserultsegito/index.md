---
title: "Mozgássérült segítő kutya"
date: "2020-06-20"
description: "A mozgáskorlátozott személyt mindennapi tevékenységeinek ellátásában segítő feladatokra kiképzett kutya."
category: "Tudástár"
tags:
  - tudastar
---

A mozgáskorlátozott személyt mindennapi tevékenységeinek
ellátásában segítő feladatokra kiképzett kutya, aki a nap 24 órájában rendelkezésre áll. A
mozgássérült személy a létező legváltozatosabb gyakorlati problémákkal szembesül nap mint
nap, és ezen akadályok leküzdésében segíti a kutya direkt módon, azzal, hogy kinyitja az 
ajtót, a földről felvesz tárgyakat, villanyt kapcsol, kézbe adja a telefont vagy bármit amit a 
gazda kér. Ugyanezen akciók indirekt módon fejtenek ki pozitív érzelmi hatásokat, azáltal, hogy
a segített személy kiszolgáltatottság érzését csökkentik. 

A környezet egyfajta tehermentesítése, 
illetve az idegenekre való ráutaltság elkerülése is megvalósul. Ezek a kutyák a kerekesszékből 
való ki- és beszállást, vagy bizonytalan járás esetén annak korrekcióját is képesek 
segíteni. Segítséget tudnak hívni, ha a gazda elesne. (KEA, 2019)

#### Hivatkozások:

1. KEA Kutyával az Emberért Alapítvány honlapja, On-line megtekintés: http://www.kea-net.hu/?page=mozgasserult-segito_kutyak
