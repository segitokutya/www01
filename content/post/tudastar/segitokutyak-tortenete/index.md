---
title: "A segítő kutyák története"
date: "2020-06-20"
description: "A legkorábbi tárgyi emlék, ami ilyen jellegű együttműködésről tanúskodik egy az 1. századból ránk maradt freskó, amely egy vakvezető kutyát ábrázol gazdájával az ókori római város, Herculaneum romjain."
category: "Tudástár"
tags:
  - tudastar
---

Hogy az emberhez való csatlakozását követően a kutya mikor jutott először segítő 
szerephez fogyatékkal élő ember mellett, pontosan nem tudjuk. A legkorábbi tárgyi emlék, ami
ilyen jellegű együttműködésről tanúskodik egy az 1. századból ránk maradt freskó, amely egy
vakvezető kutyát ábrázol gazdájával az ókori római város, Herculaneum romjain. (Cohen,2011) 
Más forrás Pompeii romokat említ. (Pawsitivity service Dogs, 2019). Hasonló jelenetet
ábrázoló középkori kínai tekercs is előkerült 1250. körülről. Az 1750-es években a párizsi 
vakok kórházában, a Quinze-Vingts-ben, már szisztematikus vakvezető kutya képzés folyt. 

Néhány évtizeddel később egy osztrák látássérült férfiról, Josef Reisingerről maradtak fenn 
feljegyzések, nevezetesen, hogy olyan jól sikerült vakvezető kutyájának kiképzése, hogy 
környezete kételkedni kezdett látássérülésében. A szintén osztrák Johann Wilhelm Klein, a bécsi
Vakok Oktatási Intézetének alapítója 1819-ben publikálta az egyik első vakvezető kutyák
képzésére vonatkozó kézikönyvet. Egyebek közt elsőként említi a merev „U” alakú hámot is,
aminek használata vagy 100 évvel később terjedt el. (Pawsitivity Service Dogs, 2019) 

A modern vakvezető kutya mozgalom az első világháború utáni Németországból indult, ahol több
ezer mustárgáz által megvakított háborús veterán élt. Beszámolók szerint egy Gerhard 
Stalling nevű orvos egy időre egyik páciens mellett hagyta német juhászát, majd dolga végeztével
visszatérve azt tapasztalta, hogy a kutya a látássérült fiatalember mellé szegődött, védeni
kezdte. Stalling a német mentőszövetség elnökeként a háború alatt több 100 collie kiképzését
követte nyomon, akik sebesült katonákat keresték vagy üzenetet vittek a frontvonalakon.
1916-ban indítványozta a kutyák átképzését vak veteránok segítésére. 1923-ban Potsdamban a
német juhászkutya szövetség oktatási központot nyitott, ami felkeltette a jómódú amerikai
rendőrkutya kiképző Dorothy Harrison Eustis érdeklődését, aki egy látogatás után 1927-ben
lelkesen ecsetelte tapasztalatait a The Saturday Evening Post-ban. Erre reagálva egy 19 éves
látássérült, Morris Frank kérte, hogy segítse kutyához jutni, akitől függetlenséget, normális
életet, munkájához való visszatérést remélte. 1929-ben Eustis és Frank megalapították az első
amerikai segítőkutyaképző iskolát The Seeing Eye néven. Öt évvel később Angliában is 
megalapították az első Vakvezető Kutya Egyesületet. Az ezt követő évtizedekben a világ számos
országában sorra nyíltak a kiképző központok. Napjainkban segítő kutyák a legkülönfélébb
területeken állnak helyt gazdáik mellett: látás és hallássérültek segítői, epilepsziás és más 
rohamok jelzőiként, autista vagy éppen szorongással, esetleg poszttraumás stressz szindrómával
küzdő emberek hű társaként. (Cohen, 2011)

Az 1990-ben elfogadott Americans with Disabilities Act (ADA), fogyatékkal élőkről
szóló USA törvényben fogalmazódott meg, hogy segítő kutyáknak van létjogosultságuk a 
látás- és hallássérülésen túl is.

Hazánkban is a vakvezető kutyák voltak a segítő kutyák előhírnökei. Az intézményes
képzés 1976-ban indult el, Vitéz Lovag Rithnovszky Jánosnak köszönhetően, aki a csepeli
Vakvezetőkutya-kiképző Iskola alapítója. 1977-ben az akkori kormányzat bezáratta, arra 
hivatkozva, hogy túl költséges a működtetése. 1978-ban újra megnyitották, és azóta napjainkig
töretlenül működik a Magyar Vakok és Gyengénlátók Országos Szövetsége szerves részeként,
pontosabban annak szolgáltató egységeként. (MVGYOSZ, 2019)

A kutya újfajta, szociális segítő szerepben való alkalmazása – eltekintve a vakvezető
kutyáktól – Magyarországon egészen a közelmúltig szinte teljesen ismeretlen lehetősége volt
a terápiának, illetve a rehabilitációnak.

Hangjelző kutyát (hearing dog – "hallókutya") először az Egyesült Államokban kezdtek
képezni a 70-es években. 1990-ben szerepel először jogszabályban. Ez a segítőkutya típus 
sokáig teljesen ismeretlen volt hazánkban, pedig hatékony segítséget tud nyújtani azoknak, akik
csökkent mértékben, vagy egyáltalán nem hallanak. A Kutyával az Emberért Alapítvány 2011.
július 5-én vizsgáztatta le és bocsájtotta gazdája rendelkezésére Magyarország első hivatalos
hangjelző kutyáját. (KEA, 2019)

Az első magyar rohamjelző kutya szintén a Kutyával az Emberért Alapítványtól került
gazdájához 2012 novemberében. (KEA, 2019)


#### Hivatkozások:

1. Cohen, J. (2011): Assistance Dogs: Learning New Tricks for Centuries, On-line megtekintés:
   (https://www.history.com/news/assistance-dogs-learning-new-tricks-for-centuries)
2. Pawsitivity Service Dogs: History of service dogs, On-line megtekintés: 
   https://www.pawsitivityservicedogs.com/history_of_service_dogs
3. Americans with Disabilities Act: https://adata.org/learn-about-ada
4. Magyar Vakok és Gyengénlátók Országos Szövetsége honlapja (MVGYOSZ): http://www.mvgyosz.hu
5. KEA Kutyával az Emberért Alapítvány honlapja, On-line megtekintés: http://www.kea-net.hu
