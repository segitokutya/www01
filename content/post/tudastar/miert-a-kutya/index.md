---
title: "Miért a kutya?"
date: "2020-06-20"
description: "Az ember és a kutya között megfigyelhető társas viselkedési párhuzamok magyarázzák a kutya alkalmasságát mindazokra a feladatokra, amelyeket napjainkban ellát."
category: "Tudástár"
tags:
  - tudastar
---

Az ember és a kutya között megfigyelhető társas viselkedési párhuzamok magyarázzák
a kutya alkalmasságát mindazokra a feladatokra, amelyeket napjainkban ellát. A 
domesztikáció során olyan kifinomult társas készségekre és kommunikációs képességekre tett szert, 
amelyek az állatvilágban páratlanok (Köböl, Hevesi és Topál, 2013). 

A kutya ilyen szempontú 
sikeressége köszönhető az emberhez való vonzódásnak, annak, hogy már kölyökkortól preferálják 
az ember jelenlétét, keresik vele a szemkontaktust, illetve a szülő-gyermek kapcsolathoz
hasonló kötődést képesek az emberrel kialakítani, ráadásul nem csak kölyökkorban, nincs ún.
érzékeny szakasza, a felnőtt állat is képes új kötődés kialakítására (Topál, 2009; Csányi, 2012;
Köböl, Hevesi és Topál, 2013). Mindez a farkasokról nem mondható el. 

A problémamegoldó
képességet illetőleg a kutyában kialakult egy szociális függőség, illetve együttműködésre való
beállítottság, aminek következtében problémamegoldáskor a farkasokhoz képest önállótlan,
az embertől vár segítséget, szociális referenciaként használja. Keresi a szemkontaktust, 
kommunikációt kezdeményez „kérdez”. Ez tette képessé – megfelelő szocializáció és képzés 
mellett – az emberrel való komplementer kooperációra, ami az a fajta együttműködés, amelynél a
felek egymást kiegészítve aktívan vesznek részt, az irányítás pedig felváltva kerül az egyik,
illetve másik félhez (Köböl, Hevesi és Topál, 2013). 

Az érzelmi-élettani szinkronizáció alatt
azt az egyébként régóta megfigyelt jelenséget értjük, hogy a kutya képes átvenni az ember
lelkiállapotát, illetve a kutyával való testi kontaktus pozitív hatással van az emberre 
fiziológiás és pszichés értelemben. A kutya meglehetősen fejlett az ember viselkedésének 
megfigyelése és annak hasznosítása terén is. A szociális tanulás, avagy a társaktól való tanulás a kutyák-
nál ugyanúgy működőképes akár embert figyel megy, akár egy fajtársát. (Miklósi és Topál, 2006) 

A kutyákra jellemző egyfajta szokás-elsajátítás, ami szintén szociális tanulás. Ennek a
lényege, hogy a sorozatos demonstrációk hatására a megfigyelő már megjósolja a másik 
viselkedését, és ezt maga is produkálni kezdi, vagy az annak megfelelő, azt kiegészítő 
viselkedést. (Miklósi és Topál, 2006). Ez a „szociális elvárás”-nak is nevezett jelenség egy adaptációs
mechanizmus, melynek lényege, a konfliktus minimalizálás, mégpedig a komplex emberi 
társas környezethez való alkalmazkodás kényszere miatt (Köböl, Hevesi és Topál, 2013). Ezek
csoportszinkronizáló viselkedések, amelyek szükségesek a csoporttársak viselkedésének
összehangolásához. Ilyenek a rítusok is. 

A kutyák hangjelzések, illetve az ember 
testjelzéseinek felismerése és értelmezése terén is sikeresek. Képes felismerni és felhasználni az ember
különböző irányjelző gesztusait, nemcsak a kézzel, hanem a lábbal való mutatást, fejbólintást,
szemmozgást. Ezenkívül a kutyák fogékonyak a tanítási helyzetekre is, felismerik azokat, 
reagálnak a figyelemfelhívó, a közlési szándékot kifejező jelzésre (most valami fontos 
következik) és a ráutaló referenciális jelzésekre (arra vonatkozik a fontos közlés) is (Topál, 2009).

#### Hivatkozások:

1. Köböl Erika, Hevesi Tímea Mária és Topál József (2013): Állatasszisztált foglalkozások.
   „Mentor(h)áló 2.0 Program” TÁMOP-4.1.2.B.2-13/1-2013-0008 projekt. On-line megtekintés:
   http://www.jgypk.hu/mentorhalo/tananyag/Allatasszisztalt_foglalkozasV2/index.html
2. Topál József (2009): Kutya és ember párhuzamok: az emberi kommunikációra való érzékenység
   evolúciója. Magyar Tudomány, 170. 11. sz. 1395-1404.
3. Csányi Vilmos (2012): A kutyák szőrös gyerekek.Libri Kiadó, Budapest.
4. Miklósi Ádám, Topál József (2006): Kutyagondolatok nyomában. Typotex, Budapest.
