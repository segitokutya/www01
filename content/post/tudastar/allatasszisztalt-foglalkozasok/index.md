---
title: "Állatasszisztált foglalkozások"
date: "2020-06-21"
description: "Az állatasszisztált terápiáknak az elméleti alapjait keressük, gyakorlati kereteit tisztázzuk, s beleillesszük a pedagógiai/gyógypedagógiai gondolkodásba."
category: "Tudástár"
tags:
  - tudastar
  - allatasszisztalt
---

Az [SZTE JGYPK Mentor(h)áló Pedagógiai Szolgáltató Központ](http://www.jgypk.hu/mentorhalo/) keretein belül
készült el az [Állatasszisztált foglalkozások](http://www.jgypk.hu/mentorhalo/tananyag/Allatasszisztalt_foglalkozasV2/) 
tananyag.
