---
title: "Személyi segítőkutya"
date: "2020-06-20"
description: "A fogyatékos személyt önálló életvitelében segítő feladatokra kiképzett kutya."
category: "Tudástár"
tags:
  - tudastar
---

A személyi segítő kutyák olyan felnőtteknek és gyermekeknek segítenek, akiknek 
fejlesztésre van szükségük valamilyen sérülés, veleszületett fogyatékosság, 
akadályozottság miatt. Ezek a kutyák elsősorban a gazda igényeire szabott
fejlesztő feladatok ellátására vannak kiképezve.

A személyi segítők ugyanazt a feladatot végzik, mint a terápiás kutyák, de mindezt 
a gazdi otthonában, folyamatosan. A terápiák alatt látszik a célszemélyeken, hogy 
mennyivel lelkesebbek, úgy végzik a feladatokat, hogy közben észre sem veszik, 
hogy dolgoznak. A kutya nagyon jó motivációs eszköz. A legtöbb intézményben heti egy 
legfeljebb két alkalommal vannak terápiák, sajnos a lehetőségek végesek, és magasabb 
csoportlétszám mellett nem túl sok idő jut egy személyre. 

A személyi segítő kutya 
mindezekre képes, de mindig kéznél van, napi rendszerességgel segíti a fejlesztést, 
ami így látványosan felgyorsulhat. A fejlesztési programot terapeuta segítségével 
dolgozzuk ki a jelentkezővel és családjával együtt, a képzés megkezdése előtt 
és segítünk a gazdának, hogyan kivitelezze ezeket. Mindemellett a 
kutya jelenléte is igazi segítség, hiszen felvidítja, mozgásra készteti gazdáját. 
Feltétel nélküli szeretete, és odaadása nagyon sokat jelent a sérült embereknek.
Önbizalmat, biztonságérzetet ad folyamatos jelenlétével.

Az jogszabály értelmében (a segítő kutya kiképzésének, vizsgáztatásának és 
alkalmazhatóságának szabályairól) „a segítő kutya: a fogyatékossággal élő személyt az 
egyenlő esélyű hozzáféréshez fűződő joga gyakorlásában, önálló életvitelének elősegítésében, 
illetve veszélyhelyzet elhárításában segítő, habilitációs, rehabilitációs
feladatokat ellátó, a külön jogszabályban meghatározott állat-egészségügyi követelményeknek
megfelelő kutya.” A személyi segítő kutyát a fogyatékos személyt önálló életvitelében segítő feladatokra
kiképzett kutyaként határozza meg.
