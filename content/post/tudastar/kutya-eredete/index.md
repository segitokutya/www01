---
title: "A kutya eredete"
date: "2020-06-20"
description: "Ma már genetikai és viselkedéstudományi vizsgálatok bizonyítják, hogy a farkas és a kutya közös őstől származik."
category: "Tudástár"
tags:
  - tudastar
---

Ma már genetikai és viselkedéstudományi vizsgálatok bizonyítják, hogy a farkas és a
kutya közös őstől származik. Régészeti leletek, ősi emberi sírok, amelyekben kutyák 
maradványait is megtalálták, árulkodnak róla, hogy a farkastól anatómiailag elkülöníthető kutya az
ember társaként valamikor 12-14 ezer évvel ezelőtt már létezhetett. Ez jóval megelőzte más
fajok háziasítását, és vélhetőleg spontán módon kezdődött. (Hare és Woods, 2013; Miklósi és
Topál, 2006; Csányi, 2012) Voltak olyan feltételezések, hogy a modern ember Afrikából 
Európába való érkezésekor, úgy 130 ezer évvel ezelőtt elkezdődhetett a kutya farkastól való 
elkülönülése. Genetikai vizsgálatok azt igazolták, hogy a két faj genetikai izolációja 13-20 ezer
évvel kezdődhetett. (Köböl, Hevesi és Topál, 2013; Csányi, 2012; Miklósi, 2010; Hare és
Woods, 2013) Valószínűleg kölcsönös előnyöknek köszönhető, hogy eltűrték egymás 
közelségét, megtörtént egy spontán előszelekció, amit követően úgy 10-15 ezer évvel ezelőtt az 
emberi letelepedéssel egyetemben kezdetét vette a valódi domesztikáció. (Miklósi és Topál, 2006) 

A farkast a könnyed táplálékszerzés vonzotta az ember közelébe, lassan adaptálódott az
új erőforráshoz miközben stressztűrő képessége növekedett, hozzászokott az ember 
közelségéhez. (Csányi, 2012) Az embernek megfelelt, hogy a vadászatainak melléktermékeit 
eltakarították, de a betolakodókat is jelezték, és egy idő után melegedésre is alkalmasnak bizonyultak
a hozzájuk szegődött, szelíd farkasok. A letelepedést követően házőrző, juhász- , terelő-, 
vadász- és egyéb munkakört betöltő megbecsült társakká váltak. E hasznos kutyákat az azonos
munka végzésében jeleskedő egyedekkel párosították. Ezzel kezdődhetett meg a valódi 
értelemben vett háziasítás. Jelenlegi elképzelésünk szerint azonban a mesterséges szelekciós
szempontoknál a lényeg a kutya és az ember közötti kommunikáció sikeressége volt, a minél
jobb beilleszkedés az emberi környezetbe, aminek elemei az emberhez való vonzódás, a 
kezelhetőség, a viselkedés ember általi kontrollálhatósága és a taníthatóság. (Köböl, Hevesi és
Topál, 2013; Miklósi és Topál, 2006)

Más kutatók érzelmi okokat hangsúlyoznak, nevezetesen, hogy a kutya szociális 
igényeket elégített ki, ami azt jelenti, hogy a szelekció irányát a kölyökszerű viselkedés és a 
szociális függőségre való hajlam határozta meg (Miklósi és Topál, 2006). A kutya a háziasítás 
hatására sokkal rugalmasabb viselkedésszerveződésre tett szert, idegrendszere befogadóbb lett a
társas viselkedés, de a környezeti változásokhoz való alkalmazkodás szempontjából is. A 
területvédelem és a párzás sem kötődik olyan szorosan az agresszióhoz, mint az ősnél, illetve
élethosszig fennmaradnak a kölyökszerű jegyek. (Köböl, Hevesi és Topál, 2013) 
Viselkedés-szabályzó rendszer változására utal az agresszió és a félelem önkontrollja és ember általi 
gátolhatósága. Továbbá az ún. „kommunikációs kényszer”, azaz az emberrel való együttélés és
az ebből adódó nagyon összetett együttműködésen alapuló interakciókban való részvétel 
hatására a kutya fogékonnyá vált az emberi kommunikációs jelekre. (Csányi, 2012; Köböl, Hevesi
és Topál, 2013) A kutya domesztikációja során az egyik meghatározó szelekciós szempont az
embertől való félelem leküzdése volt, azaz stressz tolerancia kifejlesztése.(Köböl, Hevesi és
Topál, 2013)

#### Hivatkozások:

1. Hare, B., Woods, V. (2013): Az okos kutya. Század Kiadó, Budapest.
2. Miklósi Ádám, Topál József (2006): Kutyagondolatok nyomában. Typotex, Budapest.
3. Csányi Vilmos (2012): A kutyák szőrös gyerekek.Libri Kiadó, Budapest.
4. Köböl Erika, Hevesi Tímea Mária és Topál József (2013): Állatasszisztált foglalkozások.
   „Mentor(h)áló 2.0 Program” TÁMOP-4.1.2.B.2-13/1-2013-0008 projekt. On-line megtekintés:
   http://www.jgypk.hu/mentorhalo/tananyag/Allatasszisztalt_foglalkozasV2/index.html
5. Miklósi Ádám (2010): A kutya viselkedés, evolúciója és kogníciója. Typotex, Budapest.
