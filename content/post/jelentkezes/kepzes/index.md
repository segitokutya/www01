---
title: "Képzések"
date: "2020-11-29"
description: "Felnőttképzés"
category: "Jelentkezés"
tags:
  - jelentkezes
  - kikepzes
---

A [Mozaik School](http://www.mozaikschool.hu/) és az AURA  Segítő Kutya Alapítvány 
Magyarországon az egyetlen államilag elfogadott kutyakiképző végzettséget adó (OKJ) 
képzését indítja:

#### Habilitációs kutya kiképző

Feltétel: érettségi megléte

* 2020. december 15., Budapest.

#### Jelző kutya kiképzője

Ráépülő képzés.

* 2020. december 5.

#### Mozgássérültet segítő kutya kiképzője

Ráépülő képzés.

* 2020. december 5.

---

Érdeklődni:
* Dr. Sztancsikné Tószegi Hajnalka [+36 70 5285637](tel:+36705285637) vagy [mozaikschool@gmail.com](email:mozaikschool@gmail.com)
* Nagy Lajos [+36 20 2258070](tel:+36202258070) vagy [itt a honlapon](/elerhetoseg/).

Bővebb tájékoztatás: [Mozaik School - Induló képzések](http://www.mozaikschool.hu/?page_id=136)

<div class="alert alert-primary" role="alert"><div class="font-weight-bold">
Tekintettel a helyzetre, arra kérjük a képzés és önkéntes
munka iránt érdeklődőket, hogy írásban vagy 
telefonon <a href="/elerhetoseg/">keressenek meg</a> minket.
</div></div>
