---
title: Szabó-Bökönyszegi Adrienn
date: "2020-11-14"
description: "Adrienn és Lego"
category: "Tagok"
tags:
  - tagok
---

![Adrienn 1](Szabó-Bökönyszegi-Adrienn.jpg)

Szabó-Bökönyszegi Adrienn vagyok, gyermek klinikai szakpszichológus. Még az egyetemi
évek elején ismerkedtem meg az Aura Segítőkutya Alapítvánnyal, azóta vagyok szerelmese a
kutyás terápiának. Egy álmom vált valóra, amikor megszereztem a Habilitációs kutyakiképző
bizonyítványt, és önkéntesként lehettem tagja a csapatnak. Amikor pedig kezembem tarthattam
a saját kiskutyámat, az maga volt a csoda. Lego azóta már vizsgázott terápiás kutya, társam
a mindennapokban és a munkámban is. Pedagógiai Szakszolgálatnál dolgozom, ahol nevelési
tanácsadással és szakértői bizottsági tevékenységgel foglalkozom. 0-18 éves korú gyerekeknek
és családjuknak segítünk Legoval, és bízunk benne, hogy ez még nagyon sokáig így is marad.

![Adrienn 2](Szabó-Bökönyszegi-Adrienn6.jpg)
