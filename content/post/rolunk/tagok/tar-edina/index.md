---
title: Tar Edina Éva
date: "2020-11-14"
description: ""
category: "Tagok"
tags:
  - tagok
---

![Edina 1](taredina.jpg)

  > "Ha egy gyerek nem tud úgy tanulni, ahogyan tanítjuk,
  > akkor úgy kell tanítanunk, ahogyan ő tanulni tud."
  > (Ignacio Estrada)

Tar Edina Éva vagyok.

Gyógypedagógus vagyok. 6 éve dolgozom autizmus 
spektrumzavarral élő gyermekekkel. Hivatásomat a 
folyamatos innovációval igyekszem színesíteni, építeni, 
hogy az érintett gyermekek mindennapjait szebbé 
varázsoljam és segítsem. Munkám során és egy kedves 
barátomnak köszönhetően találkoztam az állatasszisztált 
terápiával, mely olyan kapukat nyitott meg, melynek 
kulcsai az állatok – jelen esetben a kutyák-titokzatos 
lényében rejlik.

Jelenleg kapcsolataimmal és a munkám során szerzett 
tapasztalataimmal igyekszem a miskolci csapat munkáját 
segíteni. Terveim szerint hamarosan egy társsal bővül a 
csapat, így a munka még inkább hatékonyabb és szebbé válhat.

