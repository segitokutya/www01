---
title: Alagyi Judit
date: "2020-11-14"
description: "Tücsök"
category: "Tagok"
tags:
  - tagok
---

![Judit 1](alagyijudit_tucsok.jpg)

2010-ben rajz-vizuális kommunikáció szakos pedagógusként, 
2012-ben design- és művészet menedzserként végeztem 
tanulmányaimat. Debrecenben két évig tanultam
terápiás segítőkutya felvezetést iskolákban, fogyatékosokat 
ellátó intézetben, korai fejlesztő
központban, gyermekpszichiátrián. 2014-ben letettem az OKJ 
Habilitációs kutyakiképző
vizsgát. 2015 óta Tücsök nevű ivartalanított szuka airedale 
terrierrel dolgozom, akivel
2016-ban sikeres terápiás kutya vizsgát tettünk. 2016-ban 
elsajátítottuk a Do as I do
kutyakiképző módszert. Budapesten négy éve tartok a 
Gyengénlátók Általános Iskolájában
halmozottan sérült gyermekeknek heti rendszerességgel 
állatasszisztált terápiát, állatasszisztált aktivitást.

![Judit 2](alagyi_tucsok2.jpg)
