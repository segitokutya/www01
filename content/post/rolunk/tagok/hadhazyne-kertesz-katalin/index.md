---
title: Hadházyné Kertész Katalin
date: "2020-06-14"
description: "... új és élményekben gazdag világ"
category: "Tagok"
tags:
  - tagok
---

SZSZBMK Sántha Kálmán Szakkórház Pszichiátriai rehabilitációs osztályon dolgozom.
Évek óta foglalkoztatott márt az állatasszisztált terápia gondolata, amikor 2018-ban osztályunkon az Aura Segítő Kutya alapítványnak köszönhetően egy kutyaterápiás bemutatón vehettünk részt. Ekkor elhatároztuk, hogy osztályunkon szeretnénk ezzel a terápiás módszerrel minél több betegünk részére örömet szerezni. Mióta lehetőségem nyílt az Aura Segítő Kutya alapítványnál önkéntesként tevékenykedni, egy számomra teljesen új és élményekben gazdag világ részévé válhattam.
 
> "A kutya éppen némaságával válik mindennél értékesebbé. Társaságában az ember rátalál a lelki békére, ahol a szavak elvesztik minden jelentőségüket!"
>
> John Galsworthy

![Katalin](hadhazinekata.jpg)
