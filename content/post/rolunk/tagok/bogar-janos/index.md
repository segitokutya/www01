---
title: Bogár János
date: "2020-11-14"
description: "János és Bizsu"
category: "Tagok"
tags:
  - tagok
---

![János 1](bogarjanos.jpeg)

Nyíregyházán születtem és mióta az eszemet tudom 
mindig is volt kutyám. Bizsuka 4 éves labrador 
ivartalanított kislány kutyus. Hihetetlenül játékos, 
akinek óriási munkakedve és munkabírása van. 2019-ben 
tettünk sikeres terápiás alkalmazói vizsgát, azóta voltunk 
már idősek otthonában, óvodában, pszichiátrián és 
mondhatom, hogy mindenhol nagyon jól éreztük magunkat. 
Szeretünk sportolni, sétálni, kirándulni, neki 
tulajdonképpen mindegy mit csinálunk, csak együtt legyünk. 
Remélem,hogy még  sok helyen bizonyíthatjuk, hogy a 
kutyák  milyen csodás lények és mennyi mindenben képesek 
segíteni az embereknek.


![János 2](bogarjanos2.jpeg)
