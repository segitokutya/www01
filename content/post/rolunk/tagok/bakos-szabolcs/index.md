---
title: Bakos Szabolcs
date: "2020-11-14"
description: "Szabi és Héra"
category: "Tagok"
tags:
  - tagok
---

![Szabi 1](bakosszabolcs.jpg)

Bakos Szabolcs vagyok. Habilitációs kutya kiképző és terápiás 
kutya felvezető. 2017-ben kerültem a AURA Segítő Kutya 
Alapítvány csapatába. Egyéni és csoportos kutyás terápiás
foglalkozásokon dolgozom. Személyi segítő, terápiás és 
mozgássérültet segítő kutyákat képzek ki.


![Szabi 2](bakosszabolcs4.jpg)
