---
title: Hadházy László
date: "2020-06-20"
description: "... Bodza nagyszerű társam"
category: "Tagok"
tags:
  - tagok
---

SZSZBMK Sántha Kálmán Szakkórház Szocioterápiás csoportban vagyok Gyógyfoglalkoztató szakasszisztens.

Mindig is szerettem a kutyákat és több cikket is olvastam a kutyák gyógyító 
hatásairól. 2018. januárjában az Aura Segítő Kutya Alapítványnak köszönhetően lehetőségünk nyílt
egy kutyaterápiás bemutató megtekintésére. Ennek hatására feleségemmel elhatároztuk, hogy 
állatasszisztált terápiát szeretnénk megvalósítani osztályunkon. Jelenleg is kiképzés alatt állunk 
Bodzával a 3 éves labrador kutyánkkal. A kiképzések során megtapasztaltam milyen szoros kapcsolat 
alakulhat ki az ember és kutyája között. Bodza nagyszerű társam és bízom abban, hogy nagyon sok ember gyógyulását, 
lelki egyensúlyának megteremtését tudjuk majd megsegíteni.
