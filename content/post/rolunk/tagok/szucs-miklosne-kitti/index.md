---
title: Szűcs Miklósné Kitti
date: "2020-06-14"
description: "... gyógypedagógusként dolgozom Csillagal"
category: "Tagok"
tags:
  - tagok
---

![Kitti 1](szucsnekitti2.jpg)

Szűcsné Kitti vagyok, gyógypedagógusként dolgozom Csillag nevű, zsemleszínű labradorommal. Ő egy kis, örök szeleburdi kutyus, mind munkában és mind munkán kívül.
Szeretettel dolgozik pici babákkal, óvodásokkal, mozgássérültekkel és felnőttekkel is egyaránt. Szereti a mozgásos gyakorlatokat. Nagyon szenzitív terápiás kutyus. Három és fél éve vagyunk egymás kollégái és társai. Hálás vagyok, mert ő van nekem.

![Kitti 2](szucsnekitti4.jpg)
