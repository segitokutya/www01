---
title: Vámos Zoltán
date: "2020-11-14"
description: "Kutyabaj! Segítő kutyás fejlesztő kuckó"
category: "Tagok"
tags:
  - tagok
---

![Zoli 1](Vámos-Zoltán-önkéntes-1.jpg)

Vámos Zoltán önkéntes vagyok, segítő kutya kiképzőként 
és terápiás kutya felvezetőjeként dolgozom az Alapítványnak 
Egerben és környékén.

2019-ben létrehoztuk a “Kutyabaj! Segítő kutyás fejlesztő 
kuckó”-t Egerben, ahol egyéni és kiscsoportos fejlesztő 
órákat nyújtunk a Csapatommal. Csenge lányom a legnagyobb
segítségem a kiképzések és a foglalkozások lebonyolítása 
során egyaránt. Dolgoztunk már Gyermekotthonban, óvodában, 
iskolában is, jelenleg a Markhot Ferenc Oktatókórház
mozgásszervi rehabilitációs osztályán tartunk heti 
rendszerességgel foglalkozásokat. Szolgáltatási körünkbe 
tartozik ezen túl segítő kutyás érzékenyítő bemutatók saját
szervezésű lebonyolítása, továbbá személyi segítő, 
mozgássérültet segítő, rohamjelző valamint un. hallókutya 
képzése az Alapítvány megbízása alapján.

A kiképzésen lévő kutyákon túl két saját kiváló teljesítményt nyújtó border collie, Panda és
Simi a Kutyabaj! csapat tagja.

A mi célunk nem az, hogy profitot termeljünk, hiszen 
nem is ebből élünk. Célunk a minőségi szolgáltatás: 
a fejlődő gyermek, a profi kutya, az elégedett szülő 
vagy gazdi, tulajdonképpen az elégedett mosoly. 
Csapatunk büszke arra, hogy az Aura Segítő Kutya Alapítvány
munkájának és sikereinek részese lehet!

Tevékenységünkről bővebben is tájékozódhattok közvetlen 
elérhetőségeink bármelyikén. Vedd fel velünk a kapcsolatot, 
hogy bővebb információt nyújthassunk neked 
szolgáltatásainkról, árainkról!

Nézd meg a [bemutatkozó videónkat!](https://fb.watch/1LGVKTQmon/)

Kapcsolat:
* https://www.facebook.com/kutyabaj.eger/
* https://kutyabaj.wixsite.com/eger
* Címünk: 3300 Eger, Kisfaludy Sándor út 14.
* Telefon: (70) 318 4029

![Zoli 2](Vámos-Zoltán-önkéntes-2.jpg)
