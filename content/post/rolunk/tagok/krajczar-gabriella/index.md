---
title: Krajczár Gabriella
date: "2020-06-14"
description: "... a kutya nekem társ, barát, és munkatárs egyben"
category: "Tagok"
tags:
  - tagok
---

Krajczár Gabriella vagyok, konduktor és kutyás terapeuta. A pályafutásom több mint 35 éve kezdődött amikor is a diploma szerzést követően elhelyezkedtem Debrecenben a DE Kenézy Gyula Egyetemi Kórház Gyermek rehabilitációs osztályán, ahol jelenleg is dolgozom. Mellékállásban a Faragó utcai óvodában látom el a sajátos nevelési igényű gyermekek fejlesztését. Heti két alkalommal ott is tartunk kutyás terápiát. Megtanítjuk az óvodásoknak hogyan kell gondoskodni, törődni egy állattal. Ez a játékos tanulás hatással van az egész fejlődésükre.

Négy éve naponta játszunk különböző korcsoportú gyerekekkel, mérhetetlen türelemmel és megértéssel, szeretettel, barátságos, gyermekbarát környezetben. 0-18 éves korú gyermekek komplex korai- fejlesztését végzem, akik nem kezdtek el időben beszélni, nem ügyes a mozgásuk, nem figyelnek a felnőttre, illetve a társaikra, vagy nem köti le a játék hosszú távon. Nagy segítségemre van egy jól képzett terápiás kutya, sok-sok kisgyermek igaz barátja. 
Szabad időnkben sokat mozgunk, és tanítjuk az újonc terápiás kutya tanulókat. A kutya nekem társ, barát, és munkatárs is egyben. Szikra 5 éves ivartalan szuka, fajtája labrador retriever.





