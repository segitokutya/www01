---
title: Linczer Edit Linda
date: "2020-11-14"
description: ""
category: "Tagok"
tags:
  - tagok
---

![Linda 1](linczereditlinda3.jpg)

Linczer Edit Linda vagyok Népegészségügyi ellenőrként és 
Táplálkozástudományi szakemberként végeztem a Debreceni 
Egyetemen. Szakdolgozatom témája az Alkalmazott 
kötődéselmélet: Segítőkutyák bevonása pszichoterápiás 
folyamatokban volt, akkor döntöttem el, hogy a habilitációs 
kutyakiképzői OKJ-s tanfolyamot is elvégzem.

A szakdolgozati témámhoz kapcsolódóan lehetőségem volt a 
Kenézy Gyula Kórház és Rendelőintézet Gyermek Pszichiátriai 
Gondozó Varga utcai rendelőjében esettanulmányt készíteni a 
figyelemzavaros gyermekek és a segítő kutya kapcsolatáról. 
A kiválasztott, segítő kölyök kutyák szocializációs 
programjában részt vettem. Terápiás foglalkozásokon és 
integrált gyermek táborokban segítőkutyákat felvezettem.

![Linda 2](linczereditlinda.jpg)
