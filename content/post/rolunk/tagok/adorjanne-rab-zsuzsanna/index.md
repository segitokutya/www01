---
title: Adorjánné Rab Zsuzsanna
date: "2020-06-14"
description: "Ez egy álom foglalkozás!"
category: "Tagok"
tags:
  - tagok
---

![Zsuzsanna 1](adorjannerabzsuzsanna3.jpg)

Adorjánné Rab Zsuzsanna vagyok, 3 gyerek anyukája és 2 terápiás kutya gazdája.
2015 - től vagyok az AURA Alapítvány önkéntese. Habilitációs kutya kiképzője
és matematika-testnevelés szakos pedagógus vagyok. A 6 éves labradorommal
5 éve dolgozom , a 4 és fél éves golden retriveremmel 3 éve dolgozom.
Nagyon rutinos, tapasztalt kutyák. Iskolásokkal, óvodásokkal,
bölcsisekkel és felnőttekkel is terápiázunk. Mindenhol szeretet
fogad minket és mi is szeretettel megyünk hozzájuk. Ez egy álom
foglalkozás!

![Zsuzsanna 2](adorjannerabzsuzsanna.jpg)
