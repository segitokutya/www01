---
title: Barkóné Csurka Ilona
date: "2020-11-14"
description: "Ilona és Mézi"
category: "Tagok"
tags:
  - tagok
---

![Ilona 1](barkonecsurkailona6.jpg)

Aki jókedvre derít, aki türelemre, figyelemre, 
fegyelemre tanít. Aki csendbe hozzád bújik, ha szomorú 
vagy, aki pajkosságával folyamatosan felhívja magára a 
figyelmet, akivel mindig jó játszani. Ha kell, hallgat, 
és okos szemével csak figyel téged. De képes visszadumálni, 
reklamálni, show- t csinálni az időseinknek, amin nagyokat 
nevetnek.  Ő Mézi az én terápiás tanuló kutyám. Egy biztos 
senkit nem hagy hidegen, aki megismeri. 2018-tól dolgozom a 
Sóstói Szivárvány Idősek Otthonában Nyíregyházán, mint 
szociális segítő szakember. Mézi nem csak a saját életembe 
hozott aktivitást, vidámságot, de bizony a demens és 
pszichiátriai ellátottakéba is. Habilitációs kutyakiképzőként 
behozom magammal időnként a foglalkozásokra. Az intézményünkön 
belül lelkes Mézi rajongó tábor alakult ki, akik számon 
tartják. Mindig ad beszédtémát, lehet a régi emlékekről, 
saját kutyákról beszélgetni. Szeretik simogatni. Érte 
hajlandóak a saját helyükről kimozdulni, mozogni, játékba, 
nótázásba kapcsolódni. Sok csodát éltünk meg közösen és 
megható pillanatokat. Amikor a nem beszélő demens néni, 
széles mosollyal üdvözli, hogy „szép kutya”. A fekvő súlyos 
demens, akit mozgatni kell, próbál oldalra fordulni, hogy 
minél jobban lássa a kutyát. Amikor a szobájából ki nem 
mozduló pszichiátriai idős, a kutyával sétál. Van még hová 
fejlődnünk mind a kettőnknek. Reményeim szerint egyszer 
Méziből is vizsgázott terápiás kutya lesz és sok idős 
embernek teszi szebbé mindennapjait. Nagyszerű csapat áll a 
hátunk mögött, akiktől folyamatosan tanulhatunk. 

![Ilona 2](barkonecsurkailona4.jpg)
