---
title: Bagi Ágnes
date: "2020-11-14"
description: "Csente erőssége a puszi osztás"
category: "Tagok"
tags:
  - tagok
---

![Ágnes 1](bagiagnes4.jpg)

Bagi Ágnes vagyok. A Kenézy Gyula Kórház Gyermekrehbilitációs 
Osztályán dolgozom, mint gyógytornász. Csoportos tornák 
keretében sok hiperaktív, nem beszélő, figyelemzavaros, 
idegrendszeri éretlenséggel rendelkező gyerekkel foglalkozok, 
akik 3-9 év közöttiek. A magánrendelőben már csecsemőkortól 
kezdve felkeresnek, ahol TSMT tornát tartok, tanítok be. 
Emellett lovasterapeuta vagyok, melyben 2 saját lovam van 
segítségemre. Szakmaválasztáskor gyerekekkel és állatokkal 
szerettem volna fogllakozni, így találtam rá a lovasterápiára, 
majd a munkahelyemen bepillantást nyerhettem a kutyás 
terápiába is. Mivel épp kutyavásárlás előtt álltam, a 
terápiás lehetőség leszűkítette a kört, mit is keresek. 
Csentére esett a választás, aki egy szerelemalomból 
született fekete labrador szuka. Habitusának és 
temperamentumának köszönhetően együttműködésünk harmonikus, 
így kisebbekkel és nagyobbakkal is szívesen dolgozunk. 
Jelenleg a gyerekrehabilitáción ovis korúak foglalkozásain 
veszünk részt. Csente nagyon aktív és gyors kutya, de ha 
kell, türelmesen kivárja a feladatok végét is. Erőssége a 
puszi osztás :)


![Ágnes 2](bagiagnes2.jpg)
