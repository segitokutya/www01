---
title: Szabó Ágnes
date: "2020-11-14"
description: "Szabó Ági, Zizi és Csiga"
category: "Tagok"
tags:
  - tagok
---

![Ágnes 1](szaboagnes.jpg)

2014-ben találkoztam az állatasszisztált terápiával. 
Elragadó volt, ahogy a kiképzett kutyáknak
megnyílnak a gyerekek és átveszik a játékosságot, 
figyelmet és pontos feladat végrehajtást. Mivel nem 
vagyok pedagógus ez egy teljesen új világot nyitott 
ki számomra. Elkezdtem kiképezni a kutyámat és elvégeztem 
a habilitációs kutyakiképző tanfolyamot. Kezdtem 
belelátni, hogy a szakemberek milyen folyamatokba 
vonják be a kutyákat és milyen óriási szakmai munka 
van abban, hogy mosolyogva, játékosan fejlesztenek. 
Megtanított ez a világ arra, hogy nem a sajnálatunkra 
van szükség, hanem a munkánkra. Az első kiképzett 
kutyám Zizi sok mindenre megtanított. Együtt lépkedtünk 
minden „lépcsőre”. Amikor Csigával – egy labrador - 
bővült a csapat láthattam, hogy egy örök gyerek kutya 
mennyi nevetést visz a foglalkozásokba. Zizi a német 
juhászokra jellemző pontos feladat végrehajtást és 
türelmet képviseli, Csiga maga a jókedv, móka, kacagás. 
Hálás vagyok, hogy részt vehetek ebben a munkában.

![Ágnes 2](szaboagnes2.jpg)
