---
title: Bacsó László
date: "2020-11-14"
description: "Luna egy igazi egyéniség."
category: "Tagok"
tags:
  - tagok
---

![Laszlo 1](Bacsó-László-Luna-Apafa.JPG)

Gépészmérnökként egy új, addig ismeretlen dimenzióba 
kerültem mikor Luna kölyökként a családunk része lett. 
Gyógypedagógus feleségem inspirálására terápiás kutyának 
kezdtük őt nevelni, miután rátaláltunk az Aura Segítő Kutya 
Alapítványra. Luna 2014 tavaszán született, nyáron már 
részt vettem Debrecenben egy terápiás vizsgára felkészítő 
táborban, ahol végleg eldőlt, hogy itt a helyem.
2016-ban szereztem meg a habilitációs kutya kiképzője 
bizonyítványt, 2017. decemberében pedig végre meg lett a 
hőn áhított tanúsítvány, terápiás kutya-felvezető páros 
lettünk.

A Kalandok és Álmok Szakmai Műhely élménypedagógiai, 
táborszervező képzésein tanult
módszertani tudást sikeresen adaptálom a kutyás 
élményfoglalkozásokon a legkisebbektől a kamasz korosztályig. 
Akár egy-egy rendhagyó foglalkozáson, napköziben vagy 
táborokban is szívesen dolgozunk gyerekekkel. Tanórákon, 
logopédiai órákon, egyéni terápiás foglalkozásokon is 
kipróbáltuk már magunkat több óvodákban, általános iskolákban 
és a helyi szakszolgálatnál. Rendszeres terápiás 
foglalkozásokat Nagykőrösön a Dalmady Győző Óvoda, 
Általános Iskola és EGYMI különböző osztályaiban végzünk 
már egy ideje. Luna minden alkalommal lelkesen érkezik, 
kíváncsi a gyerekekre, szeret dolgozni, ő egy igazi 
egyéniség.


![Laszlo 2](Bacsó-László-Luna-ernyő.jpg)
