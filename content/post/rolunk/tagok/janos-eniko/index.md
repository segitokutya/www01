---
title: János Enikő
date: "2020-11-14"
description: "Enikő és Maya"
category: "Tagok"
tags:
  - tagok
---

![Enikő 1](janoseniko.jpg)

János Enikő vagyok. Mentálhigiénikusként foglalkozásaimon 
segítségemre van Maya cavalier spániel terápiás kutyám. 
Barátságos, jóságos kis munkatársammal elsősorban az autista 
fiataloknál tartok állatasszisztált foglalkozásokat, de 
mentálhigiéne óráimat is színesítem azzal, hogy beviszem 
Mayát.

Kutyáktól félő fiatalok, gyermekek esetében többször 
megfigyelhető volt, hogy egy idő múlva egyre bátrabban 
vettek részt a feladatokban.

![Enikő 2](janoseniko3.JPG)
