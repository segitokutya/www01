---
title: Renner Krisztina
date: "2020-11-14"
description: "Krisztina és Mese"
category: "Tagok"
tags:
  - tagok
---

![Krisztina 1](rennerkrisztina.jpg)

Renner Krisztina vagyok, gyógypedagógus-pszichopedagógus. 
Kolléganőmmel és barátommal, Mesével, (aki egyébként 
családunknak is nélkülözhetetlen tagja) 2014 óta 
dolgozunk együtt. 2013-tól vagyunk az Aura Segítőkutya 
Alapítvány önkéntesei.

Mese drótszőrű magyar vizsla hölgy. Kiegyensúlyozott, 
nyugodt, barátságos és nagyon kedves, mindenben lehet rá 
számítani... bár a nokedliszaggatás terén még van mit 
tanulnia...

Dombóváron és környékén tevékenykedünk, 0-99 éves korig 
vannak barátaink.

Több intézményben dolgoztunk és dolgozunk jelenleg is. 
Van, ahol rendszeresen, van ahol egy-egy meghívásnak 
teszünk eleget. Gyakran beszélünk a felelős 
állattartásról, a segítőkutyákról, vagy arról, 
hogyan viselkedjünk és kommunikáljunk kutyákkal.

Jelenleg a Tolna Megye Pedagógiai Szakszolgálat 
Dombóvári Tagintézményében vagyunk főállásban, 
emellett több óvodában, egy speciális iskolában, 
egy sérült felnőttek számára működő napköziotthonban 
és egy idősotthonban tartunk rendszeresen foglalkozásokat.

Hálás vagyok a sorsnak és mindazoknak, akik segítettek 
bennünket azon az úton, mely ehhez a munkához vezetett 
bennünket. Mert nagy örömünket leljük benne, számtalan 
barátra tettünk szert, és hab a tortán, hogy segíteni 
is tudunk. 

![Krisztina 2](rennerkrisztina9.jpg)
