---
title: Kárászné Dudás Erzsébet
date: "2020-11-14"
description: "Kutyáim Héli, Blanka és Lekvár"
category: "Tagok"
tags:
  - tagok
---

![Erzsébet 1](karasznedudaserzsebet3.jpg)

A nevem: Kárászné Dudás Erzsébet, gyógypedagógusként 
dolgozom pedagógiai
szakszolgálatnál Kalocsán, logopédia-szomatopedagógia 
szakirányon. Logopédusként
óvodákban látok el 3-5 éves megkésett beszédfejlődésű és 
nyelvi zavaros gyermekeket. E
mellett korai fejlesztés, gondozásban 0-3 éves korú 
gyermekeket fejlesztek. Lassan tíz éve
dolgozom terápiás kutyákkal. Három kutyám van: Héli, 
Blanka és Lekvár. Az évek alatt
különböző csoportokkal foglalkoztunk: korai fejlesztésben, 
SNI óvodás- és iskoláskorú
gyermekekkel, fogyatékkal élő felnőttekkel, és tartunk 
iskola előkészítő foglalkozásokat
szorongó és magatartás problémákkal küzdő gyerekeknek.

![Erzsébet 2](karasznedudaserzsebet.jpg)
