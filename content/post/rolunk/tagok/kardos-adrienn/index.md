---
title: Kardos Adrienn
date: "2020-03-04"
description: "... hivatásom van"
category: "Tagok"
tags:
  - tagok
---

![Adrienn 1](kardosadrienn.JPG)

Kardos Adrienn vagyok,

2017\. áprilisától mondhatom el magamról, hogy hivatásom van,
nem csak munkahelyem, ugyanis akkor kerültem az AURA Segítő Kutya Alapítvány csapatába.
Személyi segítő-, mozgássérültet segítő- és terápiás kutyákat képzek, valamint egyéni
és csoportos fejlesztő foglalkozásokon a terápiás kutya felvezetője vagyok.

![Adrienn 2](kardosadrienn2.JPG)
