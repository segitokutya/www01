---
title: "Terápiás helyszíneink"
description: "Ahol az AURA kutyaterápiákat tart."
date: "2020-06-20"
category: "Rólunk"
tags:
  - rolunk
  - terapia
---

* TM EGYMI Kinizsi utcai Telephelye Dombóvár
* Presidium Közhasznú Egyesület Re-ménység Napközi-otthona Dombóvár
* BKMPSZ Kalocsai Tagintézménye, 6300 Kalocsa, Kunszt u. 1.
* Szociális Alapszolgáltatási és Szakellátási Központ 6300 Kalocsa, Malom u. 5.
* Gyengénlátók Ált. Iskolája, EGYMI,  Budapest,
* Gyengénlátók Ált Isk, 1147 Budapest, Miskolci út 77. 
* Mónosbéli Gyermek otthon Mónosbél
* Markhot Ferenc Oktatókórház és Rendelőintézet, Eger
* Miskolc-saját otthon
* Sajóbábonyi Deák Ferenc Általános Iskola, Sajóbábony
* B-A-Z Megyei Kórház Gyermekrehabilitációs Osztálya, Miskolc
* BAZ Megyei Semmelweis Kórház Felnőtt Pszichiátria, Miskolc
* Miskolci Éltes Mátyás Óvoda, Ált. Iskola és EGYMI Telephely: Tüskevár Óvoda 3534 Miskolc
* Segítő Összefogás Alapítvány Hejőbába
* Nyitott Ajtó Baptista Iskola Edelény
* József úti Óvoda, Miskolc,
* Jókai Mór Református Óvoda, Miskolc
* Miskolci Éltes Mátyás Általános Iskola, Miskolc,
* B-A-Z Megyei Kp Kórház Rehabilitá-ciós oszt., Miskolc
* B-A-Z Megyei Kp. Kórház Rehabilitáci-ós osztálya, Miskolc,
* Zöldpillangó Családi Bölcsöde, Miskolc-Szirma
* SION Fejl. Nevelést Oktatást végző Intézet Nyírbogdány      
* Gyermekek Háza Déli Óvoda Nyíregy-háza
* Sánta Kálmán Pszichiátriai Szakkór ház Nagykálló
* Csigaház Korai Fejlesztő Központ Kisvárda
* Magdaléneum Fogyatékosok Ápoló, Gon-dozó Otthona Nyíregyháza
* Faragó utcai óvoda Debrecen
* Kenézy Gyula Kórház és Rendelő intézet Gyermek rehabilitációs Osztály Debrecen
* Debreceni Szociális Szolgáltató Központ, Debrecen
* Kenézy Gyula Kórház és Rendelőinté-zet Gyermekpszichi-átria Debrecen
* Debreceni Nagytemplomi Ref. Egyh. Immánuel Otthona Debrecen
* Talentum Baptista Általános Iskola, Debrecen
* Dr. Molnár István EGYMI, Hajdú böszörmény
* HBM Pedagógiai Szakszolgálat Hajdúszoboszló
* Dalmady Győző Óvoda, Általános Iskola és EGYMI, Nagykőrös
* Esély Pedagógiai Központ 5600 Békéscsaba, Vandháti út 3.
* Szegedi Dr. Waltner Károly Otthon Szeged 6723 Agyagos u.45.
* Szegedi Óvoda Klebelsberg Telepi Óvodája, Szeged
* Szeged és Térsége Bárczi Gusztáv EGYMI 6723, Szeged
