---
title: "Unsere Geschichte"
description: "Wie alles angefangen hat"
date: "2021-10-01"
category: "Rólunk"
tags:
  - rolunk
  - tortenetunk
---

## Die Idee!

Im Herbst 2002 brachten drei Förderpädagogen geistig behinderte Kinder zum Schwimmkurs. 
Sie stiegen in Debrecen in den Bus ein, wo auch ein Hund mit seinem Herrchen mitfuhr. 
Die Kinder fingen an den Hund zu streicheln, wobei das Herrchen erzählte, wie der Hund 
hieß, wie alt er war, ob er männlich oder weiblich war, wo er wohnte und was er gern 
spielte. Dann stiegen die Kinder und die drei Pädagogen aus dem Bus aus und gingen 
zum Schwimmbad. Nach dem Schwimmen warteten sie an der Bushaltestelle, stiegen in 
den Bus ein, und „zufällig“ fuhr auch wieder derselbe Hund mit seinem Herrchen mit. 
Jetzt kam die ÜBERRASCHUNG:

die Kinder (mit mittelschwerer geistiger Behinderung) erinnerten sich an alle 
Informationen zum Hund! Außerdem unterhielten sie sich sehr motiviert und offen 
mit dem Herrchen.

Dann kam die IDEE! Die drei Pädagogen beschlossen, einen Hund in die Förderung 
der Kinder mit einzubeziehen. Im Internet fanden sie einen Forscher am Lehrstuhl 
für Ethologie der Universität ELTE, den Vorsitzenden der Stiftung Mit dem Hund 
für den Menschen, Dr. József Tropál, den sie für die Verwirklichung ihrer Idee 
um Hilfe baten.

## Zufälle gibt es nicht!

Ein Hundetrainer aus Debrecen hatte es satt, Schutzhunde auszubilden, und wollte 
mit der Ausbildung von Therapiehunden anfangen. Im Herbst 2002 organisierte der 
Lehrstuhl für Ethologie der Universität ELTE einen Tag der offenen Tür, wo sie 
ein Experiment vorführten. Jugendliche machten mit einem Hund Therapie. Ihr 
Verhalten am Anfang der Therapie bzw. nach einem Jahr wurde in einem Video gezeigt.

Dann kam etwas ÜBERRASCHENDES:  Den Hundetrainer aus Debrecen hat das Ergebnis 
des Experiments „infiziert”. Er beschloss, Therapiehunde auszubilden und ein 
Institut zu finden, das sich mit der Förderung von behinderten Kindern beschäftigt, 
wo die Hunde in die Therapie mit einbezogen werden könnten. Der Trainer bat 
zunächst Dr. József Topál um Hilfe, und so trafen der Pädagoge und der 
Hundetrainer aufeinander.

## Die erste Veranstaltung

Am 15. April 2003 um 10 Uhr trafen sich die drei Förderpädagogen, vier Assistenten, 
ein Ausbilder und ein Hund (namens Sziszi) und 15 behinderte Kinder. Es war ein 
unbeschreibbares Erlebnis für alle. Zwei Stunden lang Hund streicheln, Hundetricks, 
Zielwerfen mit dem Ball und apportieren. Dieses Erlebnis sollte auf jeden Fall 
auch anderen Kindern zugänglich werden!

Die Fortsetzung: Sziszi und der ungarische Vorstehhund Rudi, der einem der Pädagogen 
gehörte, wurden zur Stiftung Mit dem Hund für den Menschen zur Prüfung für 
Therapiebegleithunde gebracht. Sie bestanden die Prüfung. Im Eiltempo wurden nun die 
Förderaufgaben an die Hunde angepasst. Wir haben viele Aufgaben ausgewählt, die 
den Kindern gefallen und sie wirksam fördern.

Im Sommer 2003 entstand die Stiftung PACSI.

Wir haben uns beworben – mit Erfolg – und haben Kinder gefördert, Therapien gemacht 
und TV-Interviews gegeben.

Dann kamen Freiwillige mit ihren Hunden, die wir ausgebildet und geprüft haben. 
Wir haben allmählich die Zielgruppe erweitert und immer mehr Kinder und ältere 
Leute ins Programm mit einbezogen.

DIE AURA: Im Frühling 2007 hielt Erik Kersting, Assistenzhund-Ausbilder am Lehrstuhl 
für Ethologie der Universität ELTE (Budapest) einen Vortrag über Anfallwarnhunde.

Erik sind wir ewig dankbar. Er hat uns nach Deutschland eingeladen (was wir sofort 
angenommen haben), wo er uns beigebracht hat, wie man einen Assistenzhund auswählt 
und dann zu einem Anfallwarnhund, einem Mobilitätsassistenzhund, einem Blindenführhund 
oder einem Therapiehund ausbildet.

Zu sehen, wie man von Welpen im Alter von 3-4 Tagen und von 6 Wochen sagen kann, 
für welche Aufgaben sie geeignet sind, war ein großartiges Erlebnis. Genauso wie 
man dem geeigneten Welpen dann beibringt, dass er für eine Warnung bei einer 
Zustandsänderung des Herrchens eine Belohnung bekommt.

Wir wollten dieses Wissen und diese Tätigkeit, die Auswahl von Anfallwarnhunden 
und ihre Ausbildung, in organisierter Form weitergeben. So haben wir 2007 die 
Assistenzhund-Stiftung AURA gegründet.

Warum gerade AURA? fragen viele.

Die Gesamtheit aller Anzeichen bei einem Epilepsieanfall nennt man im medizinischen 
Fachjargon AURA.

Im Frühling 2003 hielten vier Menschen mit zwei Hunden Förderveranstaltungen für 15 Kinder.

Im Jahre 2019 haben 44 Freiwillige in 14 ungarischen Städten jede Woche 65 
Veranstaltungen mit 30 geprüften Hunden für 750 Kinder, Jugendliche und ältere 
Menschen gehalten.

Wir haben 5 Mobilitätsassistenzhunde (Péntek, Filip, Lili, Blacky, Zsömi), 
7 Anfallwarnhunde und 7 Servicehunde (Buddha, Chaplin, Ozzy, Cifra, Oszkár, Rozi, Bigyó) 
ausgebildet.

Derzeit nehmen 11 Therapiehunde, 3 Servicehunde (Zara, Mokka, Magic), 
2 Diabetikerwarnhunde (Belle und Nózi) und ein Mobilitätsassistenzhund (Panda) an 
der Ausbildung teil.
