---
title: "Történetünk"
description: "Ahol minden elkezdődött"
date: "2020-03-06"
category: "Rólunk"
tags:
  - rolunk
  - tortenetunk
---

## Az ötlet!

2002\. őszén három fejlesztő pedagógus értelmileg akadályozott gyerekeket vitt úszni.
Felszálltak Debrecenben a buszra, ahol velük gazdájával utazott egy kutya. A gyerekek
simogatni kezdték a kutyust, közben a gazdi elmondta nekik, hogy hívják a kutyát,
hány éves, fiú vagy lány, hol lakik, mit eszik, mit szeret játszani a kutya. Majd a
gyerekek és a három pedagógus leszálltak a buszról és elmentek úszni. Úszás után
kimentek a buszmegállóba, felszálltak a buszra és „véletlenül” ugyanazon a buszon
ugyanaz a kutya és gazdája utaztak, kivel már találkoztak úszás előtt.

Ekkor jött a DÖBBENET: a gyerekek (középsúlyos értelmileg akadályozott) MINDEN
információra emlékeztek, ami a kutyára vonatkozott! Ráadásul rendkívül motiváltan,
nyitottan beszélgettek a gazdival.

Ekkor jött az ÖTLET! A három pedagógus elhatározta KUTYÁT vonnak be a fejlesztésbe.
Megtalálták az Interneten dr. Topál Józsefet az ELTE Etológia Tanszék kutatóját
és a Kutyával az Emberért Alapítvány elnökét, akitől segítséget kértek tervük
megvalósításához.

## Véletlenek nincsenek!

Egy debreceni kutyakiképző megunta az őrző-védő kutyák kiképzését és segítő kutyákat
szeretett volna képezni. 2002. őszén az ELTE Etológiai Tanszék nyílt napot
szervezett, ahol bemutatták kísérletüket. Fiatalok terápiáztak egy segítő kutyával
és bemutatták a fiatalok viselkedését a kísérlet kezdetén videón, illetve egy évvel később.

Ekkor jött a DÖBBENET: A kiképző „megfertőzte” a videón látott eredmény. Elhatározta:
terápiás kutyákat fog képezni és keres egy fogyatékkal élő gyerekek fejlesztésével
foglalkozó intézményt, ahol be tudják vonni a kutyát a fejlesztésbe. A kiképző
dr. Topál Józseftől kért segítséget az induláshoz. dr. Topál József segítségével
egymásra találtak a pedagógusok és a kiképző.

## Az első foglalkozás

2003\. április 15. 10 óra három fejlesztő pedagógus, négy asszisztens, egy kiképző
és egy kutya (Sziszi) és 15 sérült gyermek találkozott. Elmondhatatlan élmény
volt mindenkinek. 2 órán át kutyasimogatás, trükközés, labda célba dobás és
apport. Ezt folytatni kell, élmény volt!

Folytatás: Sziszit és az egyik pedagógus magyar vizsláját Rudit elvittük a
Kutyával az Emberért Alapítványhoz terápiás kutya vizsgára. A vizsga sikerült.
Őrült tempóban kezdtük el a fejlesztő feladatokat „kutyásítani”. Rengeteget
kiválasztottunk, melyik feladat tetszik a gyerekeknek, melyik fejleszti hatékonyan őket.

2003\. nyarán megszületett a PACSI Alapítvány.

Pályáztunk – nyertünk, fejlesztettünk, terápiáztunk, TV riportokat adtunk.

Majd jöttek az önkéntesek kutyáikkal, akiket képeztünk, kiképeztünk, vizsgáztattunk
és bővítve a célcsoportot egyre több gyereket, majd idős embert vontunk be a programba.

Az AURA: 2007. tavaszán Németországból Erik Kersting segítő kutya kiképző tartott
előadást az ELTE Etológia Tanszékén a rohamjelző kutyákról.

Eriknek örök hála. Meghívott minket (mi persze azonnal elfogadtuk a meghívást)
Németországba, ahol megtanította nekünk, hogyan válasszunk ki segítő kutyát és
hogyan képezzünk belőle rohamjelző, mozgássérültet segítő, vakvezető vagy
terápiás kutyát.

Hihetetlen nagy élmény volt látni, hogy hogyan lehet megállapítani egy 3-4 napos,
majd egy 6 hetes kutyakölyökről milyen feladatra lesz alkalmas. Aztán hogyan
lehet az alkalmas kölyöknek megtanítani, hogyan értse meg az összefüggést a
gazda állapotváltozása és az „ELŐJELZÉS jutalmat ér” élménye között Ez is egy
HÚ élmény volt!

Gondoltuk, ezt a tudást és tevékenységet, a rohamjelző kutyák kiválasztását és
képzését egy új szervezetben indítjuk el. 2007-ben megalapítottuk az AURA
Segítő Kutya Alapítványt.

Miért AURA? Kérdezik sokan.

Az epilepsziás roham előjeleinek összességét hívja az orvosi szakzsargon AURÁ-nak.

2003\. tavaszán 4 ember 2 kutyával 15 gyereknek tartott fejlesztő foglalkozásokat.

2019-ben 44 önkéntesünk terápiázik az ország 14 városában, hetente 65 foglalkozást
tart 30 vizsgázott terápiás kutyával 750 gyereknek, fiatalnak és idős embernek.

Kiképeztünk 5 mozgássérültet segítő (Péntek, Filip, Lili, Blacky, Zsömi),
7 rohamjelző és 7 személyi segítő kutyát (Buddha, Chaplin, Ozzy, Cifra, Oszkár, Rozi, Bigyó).

Jelenleg kiképzés alatt áll 11 terápiás kutya, 3 személyi segítő (Zara, Mokka, Magic),
2 vércukorszint jelző kutya (Belle és Nózi), 1 mozgássérültet segítő kutya (Panda).
