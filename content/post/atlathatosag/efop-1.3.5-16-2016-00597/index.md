---
title: "Projekt ismertetése"
date: "2021-01-21"
description: "Az EFOP-1.3.5-16-2016-00597. sz. projekt tartalmának rövid bemutatása"
category: "Átláthatóság"
tags:
  - atlathatosag
---

<div class="blogimage" style="float:right;width:300px;padding:10px 0 10px 10px;">
    <img src="infoblokk_kedv_final_felso_cmyk_ESZA.jpg" alt="Széchényi 2020"/>
</div>

A kedvezményezett neve: **AURA Segítő Kutya Alapítvány**

A projekt címe: **„Közösségfejlesztés, társadalmi szerepvállalás erősítése segítő kutyákkal”**

Szerződött támogatás összege: **24.703.860 Ft**

A támogatás mértéke (%-ban): **100 %**

## A projekt tartalmának rövid bemutatása:

Célunk a projekt 3 éven keresztül tartó lebonyolítása alatt 65 db színvonalas 
program/esemény/rendezvény lebonyolítása, melyek témái, formái
az életkori sajátosságokat figyelembe véve lehetnek például:
* interaktív segítőkutya-bemutató, milyen sportot tudnak játszani gazdák és kutyák együtt, érdekes trükkök mutatása a kutyákkal együtt,
játékos élményfoglalkozás- mindezek leginkább a fiatal korosztályt érinthetik,
* részvétel és tapasztalatcsere szomszédos, romániai, határmenti régió rendezvényein pl. Székelyhídon szervezett nagyrendezvényen
bemutató tartása,
* felelős állattartás alapjainak megismertetése, ezen keresztül betekintés a segítőkutyák életébe,
* a fogyatékossággal élő személyek potenciális munkatársainak érzékenyítése, befogadó-készség és a foglalkoztatás növelése, valamint a
hátrányos megkülönböztetés csökkenése érzékenyítő programok segítségével, kapcsolódás a jellemzően fogyatékkal élő zenészekből álló
együttesek éves rendezvényeihez,
* új irányként jelenhet meg az idősek, a 60 év felettiek körében végzett terápia. Ennek célja az idős személy egészségi állapotának
fenntartása, egészségi állapotában bekövetkező romlás megállítása, lelassítása. Az egyéni aktivitás minőségének és mennyiségének
növelése, az életút feldolgozásában való segítség. A szociális kapcsolatok beszűkülésének megakadályozása, a feleslegesség érzésének
csökkentése, a kommunikáció fejlesztése. Az idős ember szellemi-fizikai-lelki aktivitásának megtartása, karbantartása. Az idős emberek a 
terápiák során megismerik és megszeretik a kutyát. Fizikai-, mentális állapotuk stabilizálódik. A kutya motiváció a mozgásra, sétára.
Csökken a hospitalizáció, a depresszió, a szorongó idősek feloldódnak. A kutya felkelti érdeklődésüket, figyelmüket, társas kapcsolataik
kiszélesednek, nyitottabbakká, elfogadóbbakká válnak. a kutya jelenléte őszinte reakciókat, érzelmeket, gesztusokat vált ki. Mindezeket
kapcsolva a 14-35 éves korosztály programjaihoz, valamint a középiskolás önkéntesek által elérhető a generációk közötti távolság
csökkenése is. Ezt a tevékenységet mind városokra pl. Debrecen, mind a régió kisebb településeire pl. Bihartorda is tervezzük
kiterjeszteni havi rendszerességgel.

A projekt befejezési dátuma: **2020. 09. 30.**

A projekt azonosító száma: **EFOP-1.3.5-16-2016-00597**.
