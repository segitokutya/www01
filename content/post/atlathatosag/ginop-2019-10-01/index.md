---
title: "Projekt ismertetése"
date: "2021-01-06"
description: "A GINOP-5.1.7.17-2019-00221. sz. projekt tartalmának rövid bemutatása"
category: "Átláthatóság"
tags:
  - atlathatosag
---

<div class="blogimage" style="float:right;width:300px;padding:10px 0 10px 10px;">
    <img src="infoblokk_kedv_final_felso_cmyk_KA.png" alt="Széchényi 2020"/>
</div>

A Gazdaságfejlesztési és Innovációs Operatív Program keretén belül
a **Nemzetgazdasági Minisztérium**, mint támogató által 2017. március 30-án
meghirdetett „Társadalmi célú vállalkozások ösztönzése” című felhívás alapján az AURA
Segítő Kutya Alapítvány, mint támogatást igénylő, **GINOP-5.1.7-17-2019- 00221.** azonosító számon támogatási kérelmet nyújtott be. A Támogató döntése alapján
az **AURA Segítő Kutya Alapítvány** támogatásban részesült.

A Projekt címe: **„AURA Segítő Kutya Alapítvány segítő kutya kiképzési tevékenységének
fejlesztése”**

A Projekt megvalósításának kezdete: **2019. 10. 01.**

A Projekt fizikai befejezésének tervezett napja: **2020. 10. 01.**

A Projekt fizikai befejezésének tényleges napja: **2020. 11. 30.**

A Projekt elszámolható költsége: **20.776.830 Ft**

Ebből a támogatás összege: **19.750.000 Ft**

Saját forrás: **1.026.830 Ft**

Támogatás mértéke: **95,06 %**

## A projekt tartalmának rövid bemutatása:

Az Alapítvány szándéka az, hogy a kutyás terápia módszerét és a segítő kutyák
alkalmazásának előnyeit széles körben megismertesse és elterjessze. Az
Alapítvány 2018. év végéig 2 mozgássérült segítő, 4 epilepszia roham-jelző, 1 hypoglicaemiás
állapotot jelző és 5 személyi segítő kutyát képezett ki és adott át. 2017-ig ezen képzés
FSZK-s támogatással valósult meg, így térítésmentesen került átadásra a kliens részére
a kiképzett kutya, azonban 2018-tól már piaci alapon is elkezdődött a kutyák képzése a
megnövekedett igények és a „sorban állás” elkerülése érdekében. Jelenleg 3 személyi
segítő és egy mozgássérült-segítő kutyát képeznek piaci alapon, 16 városban tartanak
terápiás foglalkozásokat, hetente 25 intézményben, 65 terápiás foglalkozás valósul meg
24 vizsgázott felvezető-kutya párossal 550 sajátos nevelési igényű gyermeknek, fiatalnak
vagy fogyatékkal élő felnőttnek.

Az Alapítvány 35 fő beszállítóval, alvállalkozóval (elsősorban a terápiáknál bevont
szakemberek) dolgozik folyamatosan együtt, összesen 23 fő felvezetővel és 23 fő OKJ-s
végzettségű kiképzővel áll kapcsolatban. Jelenleg 50 önkéntesük van, melyből 29 fő OKJ-
s habilitációs (terápiás) kutyakiképző, 4 fő mozgássérült-segítő kutyakiképző, 2 fő
jelzőkutya kiképző végzettséggel is rendelkezik.

Az Alapítvány 2017. évi értékesítési nettó árbevételének 68 % - családi kutya képzés,
vizsgáztatás, tréning árbevételéből, 32 % kutyás terápia, kutyás bemutató –
foglalkozásból származott. Az üzleti alapon történő segítőkutya kiképzés 2018. évtől
indult el, előtte csak támogatásból valósult meg segítő kutya kiképzés, melyet
térítésmentesen adtak át a gazdának, így abból árbevétel nem származott.

A szervezet jelen projekt keretében kívánja társadalmi vállalkozását egy magasabb
szintre emelni és a megnövekedett vevői igényeket kielégíteni azzal, hogy az
alkalmazotti létszámot növeli és a szolgáltatások tekintetében egy nemzetközi minősítést
szerez, párhuzamosan az infrastrukturális feltételek fejlesztésével, az új un. „szimulációs
ház” kialakításával. Ezzel az új – külföldön már évek óta alkalmazott - módszerrel
minőségi szintet lép a segítő kutya kiképzés költséghatékonysága és időigénye. A már
ismert, saját környezetben a kutya 1 óra alatt megtanulja azt, amit az idegen
környezetben 3-4 óra alatt tanul meg (pl. speciális ajtónyitást). Ezzel az új módszerrel
egyszerre egy kiképző 2 kutyát is tud képezni.

A projekt keretében 3 fő új (hátrányos helyzetű) munkavállaló részére biztosít hosszú
távú munkajövedelmet.

A szervezet a tevékenységeit bérelt telephelyén (4031 Debrecen, Határ u. 5.) biztosítja.

A **GINOP-5.1.7-17-2019-00221.** sz. projekt részletes bemutatása:

1. A szakmai tevékenység megvalósításához szükséges eszközök beszerzése:
   berendezett „szimulációs” ház beszerzése és letelepítése a telephelyre.

2. Projektmenedzsment és szakmai vezetői tevékenység 1 fő projektmenedzser, 1 fő
   pénzügy vezető megbízási szerződéssel, 1 fő szakmai vezető munkaviszonyban történő
   foglalkoztatása

3. Kötelező nyilvánossághoz kapcsolódó és marketing-kommunikációs tevékenységek
   megvalósítása (honlapfejlesztés, Facebook oldal angol nyelvű változatának elkészítése,
   angol nyelvű videók, leírások készítése és megjelenítése, Google és Facebook kampány)

4. OKJ-s felnőttképzési tevékenység: 1 fő habilitációs kutya képző, 1 fő jelzőkutya
   kiképzés és 1 fő mozgássérült kutya kiképző OKJ-s képzése

5. 3 fő célcsoporttag 6 hónapon keresztül történő támogatott foglalkoztatása.
   Munkakörök: 3 fő segítő kutya kiképző és terápiás kutya felvezető.

#### Képek az új szimulációs házról:

![Ház 1](Szimulációs ház 1..jpg)

![Ház 2](Szimulációs ház 2..jpg)

![Ház 4](Szimulációs ház 4..jpg)

![Ház 6](Szimulációs ház 6..jpg)

![Ház 7](Szimulációs ház 7..jpg)
