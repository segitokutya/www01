---
title: "Átláthatóság"
date: "2021-01-21"
description: "Az alapítványunk átlátható pénzügyei"
category: "Átláthatóság"
tags:
  - atlathatosag
---

### Pályázataink:

- [GINOP-5.1.7.17-2019-00221](/atlathatosag/ginop-2019-10-01/)
- [EFOP-1.3.5-16-2016-00597](/atlathatosag/efop-1.3.5-16-2016-00597/)
