---
title: "Kutyaiskola és panzió"
description: "Új szolgáltatásunk"
date: "2020-11-29"
category: "Kutyasuli"
tags:
  - kutyasuli
  - szolgaltatas

---

Segítő kutyák kiképzési módszereit alkalmazva a következőket kínáljuk:

### Kutyasuli
Tanítunk **gazdát** és **kutyát**:
* akkor tanítjuk a kutyát, amikor neki kedve van tanulni, nem akkor, amikor a gazdi ráér!
* egy-egy alkalommal rövid ideig (5-15 perc/alkalom), **de sokszor** (napi 6-10 alkalom) képezzük a 
kutyusokat.
* **mindig sikerélménnyel** és játékkal zárul a képzési szituáció, így a kutyus még inkább akar 
tanulni, játszani, szabályt követni.
* nagyon tapasztalt segítőkutya kiképzők tanítanak kutyát, gazdát!
* amikor a kutyus már tudja a feladatot, megtanítjuk a gazdát is, hogyan kommunikáljon a kutyájával 
(motiváció, gátlás, vezényszavak, kéz- és testjelek, stb).

### Napközi 
Munka előtt beadod, munka után haza viheted:
* ha **unatkozik a kutyád**,
* **rossz szokásai** vannak,
* **nem érsz rá lemozgatni** nap közben.

### Bentlakásos kutyakoli 
Hétköznap beadod, hétvégére hazaviszed:
* ha **gyorsabban szeretnél haladni a kiképzéssel**,
* ha **el akarsz utazni hétvégén**,
* ha építkezel, költözöl, és **folyamatos felügyeletet szeretnél a kutyádnak** (és nem árt, ha 
közben okosodik is),
* ha **intenzívebb szocializációra van szüksége** kutyádnak,
* és persze azokban a helyzetekben, ami most nem jut eszünkbe, de Te tudod, hogy így jobb Nektek!

### Panzió
Ha már mindent tud a kutyád, amire Neked szükséged van a kellemes együttéléshez, de
* elutazol, és **nem tudod/akarod magaddal vinni kedvenced**,
* építkezel, költözöl, és **folyamatos felügyeletet szeretnél** a kutyádnak,
* és persze azokban a helyzetekben, ami most nem jutnak eszünkbe, de Te tudod, hogy így jobb Nektek!

---

<p class="text-center">
<a href="/elerhetoseg/"><button type="button" class="btn btn-primary">Vegye fel velünk a kapcsolatot!</button></a>
</p>

---

![Kutyasuli](kutyasuli.jpg)

---

<p class="text-center">
<a href="/elerhetoseg/"><button type="button" class="btn btn-primary">Vegye fel velünk a kapcsolatot!</button></a>
</p>

---

![Kutyasuli](kutyasuli-3.jpg)

![Kutyasuli](kutyasuli-2.jpg)

![Kutyasuli](kutyasuli-1.jpg)

---

*A szolgáltatást az AURA Segítőkutya Alapítvány nyújtja a saját telephelyén (4031 Debrecen, Határ út 5.).
A kutyák oda- és hazaszállítása a gazdák feladata. Előzetes egyeztetés és a kutya viselkedése 
függvényében kutya "taxizást" is vállalunk.*

---

<p class="text-center">
<a href="/elerhetoseg/"><button type="button" class="btn btn-primary">Vegye fel velünk a kapcsolatot!</button></a>
</p>

