import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Eu2020Logo = () => {
  const data = useStaticQuery(graphql`
    query {
      logo: file(
        relativePath: { eq: "infoblokk_kedv_final_felso_cmyk_KA.png" }
      ) {
        childImageSharp {
          fixed(height: 200) {
            ...GatsbyImageSharpFixed_withWebp
          }
        }
      }
    }
  `)

  return <Img fixed={data.logo.childImageSharp.fixed} />
}

export default Eu2020Logo
