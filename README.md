# AURA új honlapja

## Fejlesztése

Kell hozzá:

- node (és npm de jön vele)
- git
- gatsby-cli (globálisan `npm i -g gatsby-cli`)

Lépésenként:

1. a forrást checkoutold ki
2. `npm install`
3. `gatsby clean` (esetleg) majd `gatsby develop`
4. nyisd meg a http://localhost:8000 URL-t és ott a szájt
5. módosítsd a forrásokat
6. `git commit` majd `git push` és a netlify telepíti az frisset

Stage: https://segitokutya.netlify.com/
Final: https://segítőkutya.hu/

## Jegyzetek

Többé kevésbé ez lesz a strukúra

- HOME
  - Hírek
  - Adományozás?
  - Ahol találkozhatott?
- Rólunk
  - Történetünk
  - Tagjaink (minden tag egy oldal v felsorolás?)
    - Tagok...
  - Kutyáink (felsorolás)
  - Képzési helyszínek (felsorolás)
  - Terápiás helyszínek (felsorolás)
  - Tagságaink (felsorolás)
  - Médiamegjelenés
  - Beszámolók
- Tudástár
  - ami fel lett sorolva az nem inkább GYIK? Az nem tudástár...
- Jelentkezés
  - Jelentkezés segítő kutyáért
  - Jelentkezés kutyás terápiára
  - Jelentkezés segítő kutya érzékenyítő bemutatóra
  - Képzésre
  - Önkéntesnek
  - Középiskolai közösségi szolgálatra
- GYIK (felsorolás v egy-egy oldal?)
  - ???
- Támogatás
- Elérhetőség
- Átláthatóság
  - Beszámolók (ezek nem publikusak eleve?)
  - Pályázatok
